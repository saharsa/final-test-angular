import { Component, OnInit } from '@angular/core';
import { TodoService } from '../todo.service';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-todoform',
  templateUrl: './todoform.component.html',
  styleUrls: ['./todoform.component.css']
})
export class TodoformComponent implements OnInit {

  text:string;
  userid:string
  constructor(private todoserv:TodoService,private router:Router,public auth:AuthService) { }
  
  onSubmit(){
    this.todoserv.addtodo(this.userid,this.text);
    this.router.navigate(['/todo']);
  }

  ngOnInit(): void {
    this.auth.user.subscribe(
      user => {
        this.userid = user.uid;
       }
    )
  }

}
