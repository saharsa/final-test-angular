import { AuthService } from './../auth.service';
import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-savedoc',
  templateUrl: './savedoc.component.html',
  styleUrls: ['./savedoc.component.css']
})
export class SavedocComponent implements OnInit {
  
  category:string = "Loading..";
  selected:string;
  userid:string;

  constructor(public classify:ClassifyService,public auth:AuthService,private router: Router) { }

  
  onSubmit(){
    this.classify.addarticles(this.userid,this.classify.doc,this.selected);
    this.router.navigate(['/collection'])
    
  }

  ngOnInit(): void {
      this.classify.classify().subscribe(
        res => {
          this.category = this.classify.categories[res];
          this.selected = this.category;
        } 
      );
  
      this.auth.user.subscribe(
        user => {
          this.userid = user.uid;
         }
      )
  }

}
