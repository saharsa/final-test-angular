import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';

// Firebase
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFireAuthModule, AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
//Material
import {MatExpansionModule} from '@angular/material/expansion';
import { FormsModule }   from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';

import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { SignupComponent } from './signup/signup.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { LoginComponent } from './login/login.component';
import { ClassifyformComponent } from './classifyform/classifyform.component';
import { SavedocComponent } from './savedoc/savedoc.component';
import { CollectionClassifyComponent } from './collection-classify/collection-classify.component';
import { AlluserComponent } from './alluser/alluser.component';
import { ViewuserComponent } from './viewuser/viewuser.component';
import { TodosComponent } from './todos/todos.component';
import { TodoformComponent } from './todoform/todoform.component';


const appRoutes: Routes = [
  { path: 'signup', component: SignupComponent },
  { path: 'login', component: LoginComponent },
  { path: 'welcome', component: WelcomeComponent },
  { path: 'classify', component: ClassifyformComponent },
  { path: 'savedoc', component: SavedocComponent },
  { path: 'collection', component: CollectionClassifyComponent },
  { path: 'alluser', component: AlluserComponent },
  { path: 'reg', component: ViewuserComponent },
  { path: 'todo', component: TodosComponent },
  { path: 'formtodo', component: TodoformComponent },

  /*{ path: 'ooks', component: BooksComponent },
  { path: 'temp/:tp/:city', component: TemperatureComponent },
  */
  { path: '',
  redirectTo: '/welcome',
  pathMatch: 'full'
},  
];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    SignupComponent,
    WelcomeComponent,
    LoginComponent,
    ClassifyformComponent,
    SavedocComponent,
    CollectionClassifyComponent,
    AlluserComponent,
    ViewuserComponent,
    TodosComponent,
    TodoformComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    FormsModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    HttpClientModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, 'final-test'),
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [AngularFireAuth],
  bootstrap: [AppComponent]
})
export class AppModule { }
